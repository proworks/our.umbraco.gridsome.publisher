﻿using Newtonsoft.Json;
using Our.Umbraco.Gridsome.Publisher.Services;
using System;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Http;
using Umbraco.Core;
using Umbraco.Core.Cache;
using Umbraco.Core.Configuration;
using Umbraco.Core.Logging;
using Umbraco.Core.Mapping;
using Umbraco.Core.Persistence;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace Our.Umbraco.Gridsome.Publisher.Controller
{
    public class GridsomePublishApiController : UmbracoApiController
    {
        private static readonly string _gridsomePublishApiToken = ConfigurationManager.AppSettings["GridsomePublishApiToken"];
        private static readonly Regex _argPattern = new Regex(@"\{[A-Za-z0-9._:@-]+\}", RegexOptions.Compiled);
        private readonly IGridsomePublisherService _gridsomePublisherService;

        public GridsomePublishApiController(IGlobalSettings globalSettings,
                                            IUmbracoContextAccessor umbracoContextAccessor,
                                            ISqlContext sqlContext,
                                            ServiceContext services,
                                            AppCaches appCaches,
                                            IProfilingLogger logger,
                                            IRuntimeState runtimeState,
                                            UmbracoHelper umbracoHelper,
                                            UmbracoMapper umbracoMapper,
                                            IGridsomePublisherService gridsomePublisherService)
            : base(globalSettings, umbracoContextAccessor, sqlContext, services, appCaches, logger, runtimeState, umbracoHelper, umbracoMapper)
        {
            _gridsomePublisherService = gridsomePublisherService;
        }

        [HttpGet]
        public string Publish(string token)
        {
            if (string.IsNullOrWhiteSpace(_gridsomePublishApiToken) || token != _gridsomePublishApiToken) return "Not Published";

            var sb = new StringBuilder();
            void LogMessage(string level, string message, object[] args, string suffix = null)
            {
                try
                {
                    var msg = message;

                    if (args != null && args.Length > 0)
                    {
                        var idx = 0;
                        msg = string.Format(_argPattern.Replace(message, _ => $"{{{idx++}}}"), args);
                    }

                    sb.AppendLine($"{DateTime.Now.ToString("O")}: [{level}] - {msg}{suffix ?? ""}");
                }
                catch (Exception ex)
                {
                    Logger.Error<GridsomePublishApiController>(ex, "Could not log message: level={level}, message={message}, args={args}, suffix={suffix}", level, message, args == null ? "<NULL>" : JsonConvert.SerializeObject(args), suffix ?? "<NULL>");
                }
            }

            try
            {
                _gridsomePublisherService.PublishImmediately(
                    (message, args) => LogMessage("DBG", message, args),
                    (message, args) => LogMessage("INF", message, args),
                    (ex, message, args) => LogMessage("ERR", message, args, ex?.ToString()));
            }
            catch (Exception ex)
            {
                LogMessage("ERR", "An unhandled exception occurred", null, ex.ToString());
            }

            return sb.ToString();
        }
    }
}
