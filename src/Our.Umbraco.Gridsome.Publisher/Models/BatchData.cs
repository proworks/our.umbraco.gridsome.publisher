﻿using System;
using Umbraco.Core.Models.Membership;

namespace Our.Umbraco.Gridsome.Publisher.Models
{
    internal class BatchData
    {
        internal BatchData(IUser user)
        {
            User = new SimpleUser
            {
                Email = user?.Email,
                Id = user?.Id ?? 0,
                Key = user?.Key ?? Guid.Empty,
                Name = user?.Name
            };
        }

        public SimpleUser User { get; }
    }
}
