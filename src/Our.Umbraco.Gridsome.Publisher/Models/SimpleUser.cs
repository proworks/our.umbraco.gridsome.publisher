﻿using Newtonsoft.Json;
using System;

namespace Our.Umbraco.Gridsome.Publisher.Models
{
    internal class SimpleUser
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("key")]
        public Guid Key { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
    }
}
