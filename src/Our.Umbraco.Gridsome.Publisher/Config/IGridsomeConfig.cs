﻿namespace Our.Umbraco.Gridsome.Publisher.Config
{
    public interface IGridsomeConfig
    {
        /// <summary>
        /// Any additional arguments that should be passed to the Gridsome build command.  Defaults to an empty string.
        /// </summary>
        string AdditionalArguments { get; }

        /// <summary>
        /// The directory any files should be copied into after being updated by Gridsome.  If not provided, no copy will be done.
        /// </summary>
        string DestinationDirectory { get; }

        /// <summary>
        /// The file name of the NPM command.  Defaults to npm.cmd
        /// </summary>
        string NpmCommand { get; }

        /// <summary>
        /// The URL to download NPM from.  Defaults to https://nodejs.org/dist/v14.15.1/node-v14.15.1-win-x64.zip
        /// </summary>
        string NpmDownloadUrl { get; }

        /// <summary>
        /// Where to store the local working files, such as NPM and the various cache and profile files it needs.  Defaults to ~/App_Data/TEMP/Gridsome
        /// </summary>
        string LocalDirectory { get; }

        /// <summary>
        /// The directory that Gridsome will output files into.  Defaults to dist
        /// </summary>
        string OutputDirectory { get; }

        /// <summary>
        /// The directory to start the Gridsome build command in
        /// </summary>
        string WorkingDirectory { get; }
    }
}
