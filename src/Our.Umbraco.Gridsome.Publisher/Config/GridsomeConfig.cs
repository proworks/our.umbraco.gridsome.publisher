﻿using System.Configuration;

namespace Our.Umbraco.Gridsome.Publisher.Config
{
    public class GridsomeConfig : IGridsomeConfig
    {
        private const string ConfigPrefix = "Gridsome:";

        public string AdditionalArguments { get; } = ConfigurationManager.AppSettings[ConfigPrefix + nameof(AdditionalArguments)] ?? "";
        public string DestinationDirectory { get; } = ConfigurationManager.AppSettings[ConfigPrefix + nameof(DestinationDirectory)];
        public string NpmCommand { get; } = ConfigurationManager.AppSettings[ConfigPrefix + nameof(NpmCommand)] ?? "npm.cmd";
        public string NpmDownloadUrl { get; } = ConfigurationManager.AppSettings[ConfigPrefix + nameof(NpmDownloadUrl)] ?? "https://nodejs.org/dist/v14.15.1/node-v14.15.1-win-x64.zip";
        public string LocalDirectory { get; } = ConfigurationManager.AppSettings[ConfigPrefix + nameof(LocalDirectory)] ?? "~/App_Data/TEMP/Gridsome";
        public string OutputDirectory { get; } = ConfigurationManager.AppSettings[ConfigPrefix + nameof(OutputDirectory)] ?? "dist";
        public string WorkingDirectory { get; } = ConfigurationManager.AppSettings[ConfigPrefix + nameof(WorkingDirectory)];
    }
}
