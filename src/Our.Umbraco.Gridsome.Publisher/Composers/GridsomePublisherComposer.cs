﻿using Our.Umbraco.Gridsome.Publisher.Config;
using Our.Umbraco.Gridsome.Publisher.Services;
using Umbraco.Core;
using Umbraco.Core.Composing;
using Umbraco.Core.Events;
using Umbraco.Core.Services;
using Umbraco.Core.Services.Implement;
using Umbraco.Web;

namespace Our.Umbraco.Gridsome.Publisher.Composers
{
    public class GridsomePublisherComposer : ComponentComposer<GridsomePublisherComponent>, IUserComposer
    {
        public override void Compose(Composition composition)
        {
            composition.RegisterUnique<IGridsomeConfig, GridsomeConfig>();
            composition.RegisterUnique<IBatchProcessorService, BatchProcessorService>();
            composition.RegisterUnique<IGridsomePublisherService, GridsomePublisherService>();

            base.Compose(composition);
        }
    }

    public class GridsomePublisherComponent : IComponent
    {
        private readonly IBatchProcessorService _batchProcessorService;
        private readonly IGridsomePublisherService _gridsomePublisherService;

        public GridsomePublisherComponent(IBatchProcessorService batchProcessorService, IGridsomePublisherService gridsomePublisherService)
        {
            _batchProcessorService = batchProcessorService;
            _gridsomePublisherService = gridsomePublisherService;
        }

        public void Initialize()
        {
            UmbracoModule.EndRequest += UmbracoModule_EndRequest;
            ContentService.Published += ContentService_Published;
        }

        public void Terminate()
        {
        }

        private void UmbracoModule_EndRequest(object sender, global::Umbraco.Web.Routing.UmbracoRequestEventArgs e)
        {
            _gridsomePublisherService.Publish(_batchProcessorService.GetBatch(e.HttpContext));
        }

        private void ContentService_Published(IContentService sender, ContentPublishedEventArgs e)
        {
            _gridsomePublisherService.QueuePublish();
        }
    }
}

