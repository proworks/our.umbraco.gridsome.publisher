﻿using System;
using System.Threading.Tasks;
using Umbraco.Core.Logging;

namespace Our.Umbraco.Gridsome.Publisher.Services
{
    public interface IGridsomePublisherService
    {
        void QueuePublish();
        void Publish(object batch);
        void PublishImmediately(ILogger logger);
        void PublishImmediately(Action<string, object[]> debugLog, Action<string, object[]> infoLog, Action<Exception, string, object[]> errorLog);
    }
}
