﻿using System;
using System.Web;
using Umbraco.Core.Models.Membership;

namespace Our.Umbraco.Gridsome.Publisher.Services
{
    public interface IBatchProcessorService
    {
        void CreateBatchSafe(IUser user, DateTime eventTime);
        object CreateBatchUnsafe(IUser user);
        object CreateBatchUnsafe(IUser user, HttpContextBase context);
        object GetBatch();
        object GetBatch(HttpContextBase context);
        bool HasBatch();
        bool HasBatch(HttpContextBase context);
    }
}
