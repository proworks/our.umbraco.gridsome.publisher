﻿using Our.Umbraco.Gridsome.Publisher.Config;
using Our.Umbraco.Gridsome.Publisher.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Threading;
using System.Web.Hosting;
using Umbraco.Core.Logging;
using Umbraco.Web;

namespace Our.Umbraco.Gridsome.Publisher.Services
{
    public class GridsomePublisherService : IGridsomePublisherService
    {
        private static readonly object _lockObject = new object();
        private static long _runIndex;

        private readonly ILogger _logger;
        private readonly IUmbracoContextAccessor _umbracoContextAccessor;
        private readonly IBatchProcessorService _batchProcessorService;
        private readonly IGridsomeConfig _gridsomeConfig;

        public GridsomePublisherService(ILogger logger, IUmbracoContextAccessor umbracoContextAccessor, IBatchProcessorService batchProcessorService, IGridsomeConfig gridsomeConfig)
        {
            _logger = logger;
            _umbracoContextAccessor = umbracoContextAccessor;
            _batchProcessorService = batchProcessorService;
            _gridsomeConfig = gridsomeConfig;
        }

        public event EventHandler<CancellableDirectoriesEventArgs> Building;
        public event EventHandler<DirectoriesEventArgs> Built;
        public event EventHandler<CancellableDirectoriesEventArgs> Publishing;
        public event EventHandler<DirectoriesEventArgs> Published;

        public virtual void QueuePublish()
        {
            var uc = _umbracoContextAccessor.UmbracoContext;
            var user = uc.Security?.CurrentUser;

            if (user?.Id == null)
            {
                throw new SecurityException("You must be authenticated in the Umbraco back-office to use this method");
            }

            _logger.Debug<GridsomePublisherService>("User {name} (ID #{id}) is queuing for gridsome publish", user.Name, user.Id);

            _batchProcessorService.CreateBatchSafe(user, DateTime.Now);
        }

        public virtual void Publish(object batchData)
        {
            try
            {
                if (!(batchData is BatchData batch))
                {
                    return;
                }

                HostingEnvironment.QueueBackgroundWorkItem(new GridsomeBuildPublisher(_gridsomeConfig, this, _logger).RunGridsomeBuild);
            }
            catch (Exception ex)
            {
                _logger.Error<GridsomePublisherService>(ex, "Could not build gridsome");
            }
        }

        public virtual void PublishImmediately(ILogger logger)
        {
            var publisher = new GridsomeBuildPublisher(_gridsomeConfig, this, logger);
            publisher.RunGridsomeBuild(default);
        }
        public virtual void PublishImmediately(Action<string, object[]> debugLog, Action<string, object[]> infoLog, Action<Exception, string, object[]> errorLog)
        {
            var publisher = new GridsomeBuildPublisher(_gridsomeConfig, this, debugLog, infoLog, errorLog);
            publisher.RunGridsomeBuild(default);
        }

        protected virtual bool OnBuilding(Directories directories)
        {
            var args = new CancellableDirectoriesEventArgs(directories);
            Building?.Invoke(this, args);
            return !args.Cancel;
        }

        protected virtual bool OnPublishing(Directories directories)
        {
            var args = new CancellableDirectoriesEventArgs(directories);
            Publishing?.Invoke(this, args);
            return !args.Cancel;
        }

        protected virtual void OnBuilt(Directories directories) => Built?.Invoke(this, new DirectoriesEventArgs(directories));
        protected virtual void OnPublished(Directories directories) => Published?.Invoke(this, new DirectoriesEventArgs(directories));

        public class DirectoriesEventArgs : EventArgs
        {
            public DirectoriesEventArgs(IDirectories directories)
            {
                Directories = directories;
            }

            public IDirectories Directories { get; }
        }

        public class CancellableDirectoriesEventArgs : DirectoriesEventArgs
        {
            public CancellableDirectoriesEventArgs(IDirectories directories)
                : base(directories)
            {
            }

            public bool Cancel { get; set; }
        }

        public interface IDirectories
        {
            string Destination { get; }
            string NpmCommandPath { get; }
            string Output { get; }
            string Profile { get; }
            string Root { get; }
            string Temp { get; }
            string Working { get; }
        }

        private class GridsomeBuildPublisher
        {
            private readonly IGridsomeConfig _gridsomeConfig;
            private readonly GridsomePublisherService _gridsomePublisherService;
            private readonly Action<string, object[]> _debugLog;
            private readonly Action<string, object[]> _infoLog;
            private readonly Action<Exception, string, object[]> _errorLog;

            public GridsomeBuildPublisher(IGridsomeConfig gridsomeConfig, GridsomePublisherService gridsomePublisherService, ILogger logger)
                : this(gridsomeConfig, gridsomePublisherService,
                      (m, v) => { if (v == null) { logger.Debug<GridsomePublisherService>(m); } else { logger.Debug<GridsomePublisherService>(m, v); } },
                      (m, v) => { if (v == null) { logger.Info<GridsomePublisherService>(m); } else { logger.Info<GridsomePublisherService>(m, v); } },
                      (e, m, v) => {
                          if (e == null) { if (v == null) { logger.Error<GridsomePublisherService>(m); } else { logger.Error<GridsomePublisherService>(m, v); } }
                          else { if (v == null) { logger.Error<GridsomePublisherService>(e, m); } else { logger.Error<GridsomePublisherService>(e, m, v); } }
                      })
            {
                _gridsomeConfig = gridsomeConfig;
                _gridsomePublisherService = gridsomePublisherService;
            }

            public GridsomeBuildPublisher(IGridsomeConfig gridsomeConfig, GridsomePublisherService gridsomePublisherService, Action<string, object[]> debugLog, Action<string, object[]> infoLog, Action<Exception, string, object[]> errorLog)
            {
                _gridsomeConfig = gridsomeConfig;
                _gridsomePublisherService = gridsomePublisherService;
                _debugLog = debugLog;
                _infoLog = infoLog;
                _errorLog = errorLog;
            }

            public void RunGridsomeBuild(CancellationToken token)
            {
                try
                {
                    token.ThrowIfCancellationRequested();
                    var myIdx = Interlocked.Increment(ref _runIndex);

                    lock (_lockObject)
                    {
                        token.ThrowIfCancellationRequested();
                        if (myIdx != Interlocked.Read(ref _runIndex)) return; //This means there is another instance waiting to run, so it will incorporate any changes this instance needed to build

                        BuildGridsome(token);
                    }
                }
                catch (Exception ex)
                {
                    _errorLog(ex, "Was not able to successfully execute gridsome build command", null);
                }
            }

            protected virtual void BuildGridsome(CancellationToken token)
            {
                var dirs = new Directories(_debugLog, _gridsomeConfig, token);

                // You must run `npm install --global --production windows-build-tools` if the MSBuild tools are not already on the server
                if (!_gridsomePublisherService.OnBuilding(dirs)) return;

                var existing = Directory.Exists(Path.Combine(dirs.Working, "node_modules"))
                    && Directory.Exists(Path.Combine(Path.Combine(dirs.Working, "node_modules"), "gridsome"))
                    && Directory.EnumerateFiles(Path.Combine(Path.Combine(dirs.Working, "node_modules"), "gridsome")).Any();

                if (!existing) existing = RunNpm(dirs, "NPM install", "install", "", token);
                if (!existing) return;

                if (!RunNpm(dirs, "Gridsome build", "run build", _gridsomeConfig.AdditionalArguments, token)) return;

                _gridsomePublisherService.OnBuilt(dirs);

                PublishOutput(dirs, token);
            }

            protected virtual bool RunNpm(Directories dirs, string processName, string command, string additionalArguments, CancellationToken token)
            {
                token.ThrowIfCancellationRequested();

                var arguments = $"{command} --scripts-prepend-node-path true {additionalArguments}".Trim();

                var log = new StringBuilder();

                using (var process = new Process())
                {
                    process.StartInfo.FileName = dirs.NpmCommandPath;
                    process.StartInfo.Arguments = arguments;
                    process.StartInfo.WorkingDirectory = dirs.Working;
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.CreateNoWindow = true;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.RedirectStandardError = true;
                    process.StartInfo.Environment["TEMP"] = dirs.Temp;
                    process.StartInfo.Environment["TMP"] = dirs.Temp;
                    process.StartInfo.Environment["USERPROFILE"] = dirs.Profile;
                    process.StartInfo.Environment["APPDATA"] = Path.Combine(dirs.Profile, "APPDATA");
                    process.StartInfo.Environment["LOCALAPPDATA"] = Path.Combine(dirs.Profile, "LOCALAPPDATA");
                    process.StartInfo.Environment["XDG_DATA_HOME"] = Path.Combine(dirs.Profile, ".local");
                    process.StartInfo.Environment["XDG_CONFIG_HOME"] = Path.Combine(dirs.Profile, ".local");
                    process.StartInfo.Environment["XDG_CACHE_HOME"] = Path.Combine(dirs.Profile, ".cache");
                    process.OutputDataReceived += (s, e) => log.AppendLine(e.Data);
                    process.ErrorDataReceived += (s, e) => log.AppendLine(e.Data);

                    _debugLog($"Starting the {processName}", null);
                    process.Start();
                    process.BeginOutputReadLine();
                    process.BeginErrorReadLine();

                    while (!token.IsCancellationRequested && !process.WaitForExit(1000)) ;

                    var success = process.HasExited && process.ExitCode == 0;
                    if (token.IsCancellationRequested && !process.HasExited) { process.Kill(); process.WaitForExit(); }

                    var procDetail = "\r\nCommand: {command}\r\nArguments: {arguments}\r\nWorking Directory: {workingDirectory}\r\nReturn Code: {code}\r\nOutput:\r\n{output}";
                    if (success)
                    {
                        _infoLog($"Successfully completed the {processName}", null);
                        _debugLog($"{processName} details{procDetail}", new object[] { dirs.NpmCommandPath, arguments, dirs.Working, process.ExitCode, log });
                    }
                    else
                    {
                        _errorLog(null, $"The {processName} process failed{procDetail}", new object[] { dirs.NpmCommandPath, arguments, dirs.Working, process.ExitCode, log });
                    }

                    return success;
                }
            }

            protected virtual void PublishOutput(Directories dirs, CancellationToken token)
            {
                token.ThrowIfCancellationRequested();
                if (string.IsNullOrWhiteSpace(dirs.Destination) || !_gridsomePublisherService.OnPublishing(dirs)) return;

                var output = Path.Combine(dirs.Working, dirs.Output);
                _debugLog("Starting publishing from {source} to {destination}", new object[] { output, dirs.Destination });
                Directory.CreateDirectory(dirs.Destination);

                var knownFiles = new List<string>();
                var outputLength = output.Length + 1;
                var destinationLength = dirs.Destination.Length + 1;

                foreach (var path in Directory.EnumerateFiles(output, "*", SearchOption.AllDirectories))
                {
                    var relativePath = path.Substring(outputLength);
                    var destinationPath = Path.Combine(dirs.Destination, relativePath);
                    var destinationDir = Path.GetDirectoryName(destinationPath);

                    knownFiles.Add(relativePath);

                    token.ThrowIfCancellationRequested();
                    Directory.CreateDirectory(destinationDir);

                    if (File.Exists(destinationPath))
                    {
                        _debugLog("Deleting before replacing {path}", new object[] { destinationPath });
                        File.Delete(destinationPath);
                    }

                    _debugLog("Copying from {source} to {destination}", new object[] { path, destinationPath });
                    File.Move(path, destinationPath); // Do a move as it will be faster on a local file system, and the same speed for a remote one
                }
                _debugLog("Updated {count} files", new object[] { knownFiles.Count });

                var toRemove = Directory.EnumerateFiles(dirs.Destination, "*", SearchOption.AllDirectories).Where(x => !knownFiles.Contains(x.Substring(destinationLength))).ToList();
                foreach (var path in toRemove)
                {
                    token.ThrowIfCancellationRequested();
                    _debugLog("Deleting obsolete file {path}", new object[] { path });
                    File.Delete(path);

                    var dir = Path.GetDirectoryName(path);
                    while (!Directory.EnumerateFiles(dir).Any())
                    {
                        _debugLog("Deleting empty directory {path}", new object[] { dir });
                        Directory.Delete(dir);
                        dir = Path.GetDirectoryName(dir);
                    }
                }
                _debugLog("Removed {count} files", new object[] { toRemove.Count });

                _gridsomePublisherService.OnPublished(dirs);
                _debugLog("Published to {destination}", new object[] { dirs.Destination });
            }
        }

        protected class Directories : IDirectories
        {
            public Directories(Action<string, object[]> debugLog, IGridsomeConfig config, CancellationToken token)
            {
                Output = config.OutputDirectory;
                Destination = GetDirectory(config.DestinationDirectory);
                Root = GetDirectory(config.LocalDirectory);
                Working = GetDirectory(config.WorkingDirectory);

                Temp = Path.Combine(Root, "temp");
                Profile = Path.Combine(Root, "profile");
                NpmCommandPath = GetNpmCommandPath(debugLog, config.NpmCommand, config.NpmDownloadUrl, token);

                EnsureDirectories(Temp);
            }

            public string Destination { get; }
            public string NpmCommandPath { get; }
            public string Output { get; }
            public string Profile { get; }
            public string Root { get; }
            public string Temp { get; }
            public string Working { get; }

            private void EnsureDirectories(params string[] directories)
            {
                foreach (var directory in directories)
                {
                    if (string.IsNullOrWhiteSpace(directory))
                    {
                        throw new Exception("directory was not provided");
                    }

                    Directory.CreateDirectory(directory);
                }
            }

            private string GetNpmCommandPath(Action<string, object[]> debugLog, string npmCommand, string npmDownloadUrl, CancellationToken token)
            {
                var npmCommandFolder = Path.Combine(Root, "npm");
                var npmCommandPath = Path.Combine(npmCommandFolder, npmCommand);

                if (!File.Exists(npmCommandPath))
                {
                    token.ThrowIfCancellationRequested();

                    debugLog("Downloading NPM to run the Gridsome build from {url}", new object[] { npmDownloadUrl });
                    if (Directory.Exists(npmCommandFolder)) Directory.Delete(npmCommandFolder, true);
                    var zipDir = Path.Combine(Root, "npm-unzip");
                    var zipFile = Path.Combine(Root, "npm-download.zip");
                    using (var client = new WebClient())
                    {
                        client.DownloadFile(npmDownloadUrl, zipFile);
                    }

                    token.ThrowIfCancellationRequested();
                    ZipFile.ExtractToDirectory(zipFile, zipDir);

                    token.ThrowIfCancellationRequested();
                    var npmCmd = Directory.GetFiles(zipDir, npmCommand, SearchOption.AllDirectories).FirstOrDefault();
                    if (npmCmd == null) throw new ArgumentException($"Could not find the command {npmCommand} in the download from {npmDownloadUrl}");

                    token.ThrowIfCancellationRequested();
                    Directory.Move(Path.GetDirectoryName(npmCmd), npmCommandFolder);
                }

                return npmCommandPath;
            }

            private string GetDirectory(string dir)
            {
                if (string.IsNullOrWhiteSpace(dir)) return null;

                if (dir.StartsWith("~/") && dir.Length > 2) dir = Path.Combine(HostingEnvironment.MapPath("~/"), dir.Substring(2));
                else if (dir[0] == '~') dir = HostingEnvironment.MapPath(dir);

                dir = Path.GetFullPath(dir);

                return dir;
            }
        }
    }
}
