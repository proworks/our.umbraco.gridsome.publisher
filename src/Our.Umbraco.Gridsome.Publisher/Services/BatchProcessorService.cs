﻿using Our.Umbraco.Gridsome.Publisher.Models;
using System;
using System.Web;
using Umbraco.Core.Logging;
using Umbraco.Core.Models.Membership;
using Umbraco.Web;

namespace Our.Umbraco.Gridsome.Publisher.Services
{
    internal class BatchProcessorService : IBatchProcessorService
    {
        private const string BatchKey = "Our.Umbraco.Gridsome.Publisher:Batch";

        private static readonly object batchLock = new object();
        private static DateTime lastBatchCreated;

        private readonly ILogger _logger;
        private readonly IUmbracoContextAccessor _umbracoContextAccessor;

        public BatchProcessorService(ILogger logger, IUmbracoContextAccessor umbracoContextAccessor)
        {
            _logger = logger;
            _umbracoContextAccessor = umbracoContextAccessor;
        }

        public void CreateBatchSafe(IUser user, DateTime eventTime)
        {
            lock (batchLock)
            {
                if (eventTime > lastBatchCreated)
                {
                    lastBatchCreated = DateTime.Now;
                    CreateBatchUnsafe(user);
                    _logger.Debug<BatchProcessorService>($"Create gridsome publish batch at {lastBatchCreated}");
                }
                else
                {
                    _logger.Debug<BatchProcessorService>($"Skip batch creation. Event is stale. Event DateTime: {eventTime}. Last Batch Created at:  {lastBatchCreated}");
                }
            }
        }

        public object CreateBatchUnsafe(IUser user)
        {
            try
            {
                var context = _umbracoContextAccessor.UmbracoContext.HttpContext ?? (HttpContext.Current != null ? new HttpContextWrapper(HttpContext.Current) : null);
                return CreateBatchUnsafe(user, context);
            }
            catch (Exception ex)
            {
                _logger.Error<BatchProcessorService>(ex, "Could not create batch data");
                return null;
            }
        }

        public object CreateBatchUnsafe(IUser user, HttpContextBase context)
        {
            try
            {
                if (context == null)
                {
                    throw new ArgumentNullException(nameof(context));
                }
                if(user == null)
                {
                    throw new ArgumentNullException(nameof(user));
                }

                if (!(context.Items[BatchKey] is BatchData savedBatch))
                {
                    context.Items[BatchKey] = new BatchData(user);
                }

                return context.Items[BatchKey];
            }
            catch (Exception ex)
            {
                _logger.Error<BatchProcessorService>(ex, "Could not create batch data");
                return null;
            }
        }

        public object GetBatch()
        {
            var httpContext = _umbracoContextAccessor.UmbracoContext.HttpContext ?? (HttpContext.Current != null ? new HttpContextWrapper(HttpContext.Current) : null);
            return GetBatch(httpContext);
        }

        public object GetBatch(HttpContextBase context)
        {
            try
            {
                return context.Items[BatchKey] as BatchData;
            }
            catch (Exception ex)
            {
                _logger.Error<BatchProcessorService>(ex, "Could not get batch data");
                return null;
            }
        }

        public bool HasBatch()
        {
            var context = _umbracoContextAccessor.UmbracoContext.HttpContext ?? (HttpContext.Current != null ? new HttpContextWrapper(HttpContext.Current) : null);
            return HasBatch(context);
        }

        public bool HasBatch(HttpContextBase context)
        {
            var hasBatch = false;

            if (!(context?.Items[BatchKey] is BatchData))
            {
                hasBatch = true;
            }

            return hasBatch;
        }
    }
}
